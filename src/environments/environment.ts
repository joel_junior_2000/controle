// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBkI78Vdon0IfD7wjXyu_5h4w0Tff6o18k',
    authDomain: 'controle-if-a.firebaseapp.com',
    databaseURL: 'https://controle-if-a.firebaseio.com',
    projectId: 'controle-if-a',
    storageBucket: 'controle-if-a.appspot.com',
    messagingSenderId: '107833155939',
    appId: '1:107833155939:web:49f6c6794d1b8ff2623356',
    measurementId: 'G-J4080CBJB7'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
