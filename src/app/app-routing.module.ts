import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'full'
  },
  {
    path: 'auth',/*path: 'login', */
    loadChildren: /*() => import('./pages/auth/login/login.module').then( m => m.LoginPageModule)*/'./pages/auth/auth.module#AuthModule'
  },
  {
    path: 'contas',
    loadChildren: './pages/contas/contas.module#ContasModule'
  },
  {
    path: 'cadastro',
    loadChildren: () => import('./pages/contas/cadastro/cadastro.module').then( m => m.CadastroPageModule)
  },
  {
    path: 'relatorio',
    loadChildren: () => import('./pages/contas/relatorio/relatorio.module').then( m => m.RelatorioPageModule)
  },


  /*{
    path: 'forgot',
    loadChildren: () => import('./pages/auth/forgot/forgot.module').then( m => m.ForgotPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/auth/register/register.module').then( m => m.RegisterPageModule)
  },*/
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
